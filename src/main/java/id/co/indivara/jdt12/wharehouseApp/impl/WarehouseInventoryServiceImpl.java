package id.co.indivara.jdt12.wharehouseApp.impl;
import id.co.indivara.jdt12.wharehouseApp.entity.*;
import id.co.indivara.jdt12.wharehouseApp.repo.StoreInventoryRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class WarehouseInventoryServiceImpl implements WarehouseInventoryService {
    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;
    @Autowired
    StoreInventoryRepository storeInventoryRepository;

    @Override
    public ResponseEntity<WarehouseInventory> saveSupplyTransaction(Goods goodsId, Warehouse warehouseId, SuppToWarehouse supply) {
        WarehouseInventory existWarehouse = warehouseInventoryRepository.findByGoodsAndWarehouse(goodsId,warehouseId);
        WarehouseInventory warehouseInventory = new WarehouseInventory();
        if (existWarehouse != null){
            existWarehouse.setStock(existWarehouse.getStock()+supply.getTotal());
            warehouseInventoryRepository.save(existWarehouse);
        } else {
            warehouseInventory.setWarehouse(warehouseId);
            warehouseInventory.setGoods(goodsId);
            warehouseInventory.setStock(supply.getTotal());
            warehouseInventoryRepository.save(warehouseInventory);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<WarehouseInventory> saveTransferTransaction(Goods goodsId, Warehouse warehouseSrc, Warehouse warehouseDest, WarehouseToWarehouse transfer) {
        WarehouseInventory existWarehouseSrc = warehouseInventoryRepository.findByGoodsAndWarehouse(goodsId,warehouseSrc);
        WarehouseInventory existWarehouseDest = warehouseInventoryRepository.findByGoodsAndWarehouse(goodsId,warehouseDest);
        WarehouseInventory warehouseInventory = new WarehouseInventory();
        if (existWarehouseDest != null){
            existWarehouseDest.setStock(existWarehouseDest.getStock() + transfer.getTotal());
            warehouseInventoryRepository.save(existWarehouseDest);
        } else {
            warehouseInventory.setWarehouse(warehouseDest);
            warehouseInventory.setGoods(goodsId);
            warehouseInventory.setStock(transfer.getTotal());
            warehouseInventoryRepository.save(warehouseInventory);
        }
        existWarehouseSrc.setStock(existWarehouseSrc.getStock() - transfer.getTotal());
        warehouseInventoryRepository.save(existWarehouseSrc);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<WarehouseInventory> saveDeliveryTransaction(Goods goodsId, Warehouse warehouseSrc, Store storeDest, WarehouseToStore delivery) {
        WarehouseInventory existWarehouseSrc = warehouseInventoryRepository.findByGoodsAndWarehouse(goodsId,warehouseSrc);
        StoreInventory existStoreDest = storeInventoryRepository.findByGoodsAndStore(goodsId,storeDest);
        StoreInventory storeInventory = new StoreInventory();
        if (existStoreDest != null){
            existStoreDest.setStock(existStoreDest.getStock()+delivery.getTotal());
            storeInventoryRepository.save(existStoreDest);
        } else {
            storeInventory.setGoods(goodsId);
            storeInventory.setStore(storeDest);
            storeInventory.setStock(delivery.getTotal());
            storeInventoryRepository.save(storeInventory);
        }
        existWarehouseSrc.setStock(existWarehouseSrc.getStock()-delivery.getTotal());
        warehouseInventoryRepository.save(existWarehouseSrc);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
