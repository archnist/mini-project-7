package id.co.indivara.jdt12.wharehouseApp.impl;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseRepository;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService {
    @Autowired
    WarehouseRepository warehouseRepository;

    @Override
    public ResponseEntity<Warehouse> warehouseRegister(Warehouse warehouse) {
        warehouse.setWarehouseCode("wh" + (warehouseRepository.count() + 1));
        warehouse.setJoinDate(new Date());
        return new ResponseEntity<> (warehouseRepository.save(warehouse), HttpStatus.CREATED);
    }
    @Override
    public List<Warehouse> showAllWarehouses() {
        return warehouseRepository.findAll();
    }

    @Override
    public List<Warehouse> findByWarehouseName(String warehouseName) {
        return warehouseRepository.findByWarehouseName(warehouseName);
    }

    @Override
    public ResponseEntity<Warehouse> updateWarehouse(Warehouse warehouseId,Warehouse warehouseData) {
       Warehouse w = warehouseRepository.findById(warehouseId.getWarehouseId()).get();
       w.setWarehouseName(warehouseData.getWarehouseName());
       w.setLocation(warehouseData.getLocation());
       return new ResponseEntity<>(warehouseRepository.save(w),HttpStatus.CREATED);
    }
}
