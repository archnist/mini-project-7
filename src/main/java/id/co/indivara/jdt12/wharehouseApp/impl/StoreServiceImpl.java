package id.co.indivara.jdt12.wharehouseApp.impl;

import id.co.indivara.jdt12.wharehouseApp.entity.Store;
import id.co.indivara.jdt12.wharehouseApp.repo.StoreRepository;
import id.co.indivara.jdt12.wharehouseApp.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    StoreRepository storeRepository;

    @Override
    public ResponseEntity<Store> storeRegister(Store store) {
        store.setStoreCode("str" + (storeRepository.count() + 1));
        store.setJoinDate(new Date());
        return new ResponseEntity<>(storeRepository.save(store), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Store> storeUpdate(Store storeId, Store storeData) {
      Store store = storeRepository.findById(storeId.getStoreId()).get();
      store.setStoreName(storeData.getStoreName());
      store.setLocation(storeData.getLocation());
      return new ResponseEntity<>(storeRepository.save(store),HttpStatus.CREATED);
    }

    @Override
    public List<Store> showAllStores() {
        return (List<Store>) storeRepository.findAll();
    }
}
