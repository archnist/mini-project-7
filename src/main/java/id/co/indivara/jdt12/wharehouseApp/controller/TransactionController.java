package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.Transaction;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.repo.SuppToWarehouseRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.TransactionRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseToStoreRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseToWarehouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/transaction")
@RestController
public class TransactionController {
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    SuppToWarehouseRepository suppToWarehouseRepository;
    @Autowired
    WarehouseToWarehouseRepository warehouseToWarehouseRepository;
    @Autowired
    WarehouseToStoreRepository warehouseToStoreRepository;

    @GetMapping("/show/all")
    public List<Transaction> showAll(){
        return (List<Transaction>) transactionRepository.findAll();
    }

    @GetMapping("/show/{warehouse}")
    public Map<String,Object> findTransactionByWarehouse(@PathVariable("warehouse") Warehouse warehouse){
        Map list = new HashMap<>();
        list.put("Supply To Warehouse",suppToWarehouseRepository.findByWarehouse(warehouse));
        list.put("Transfer To Warehouse",warehouseToWarehouseRepository.findByWarehouseSource(warehouse));
        list.put("Delivery To Store",warehouseToStoreRepository.findByWarehouseSrc(warehouse));
        return list;
    }

}
