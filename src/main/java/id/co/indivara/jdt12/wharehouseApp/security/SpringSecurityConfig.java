package id.co.indivara.jdt12.wharehouseApp.security;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("whuser").password("{noop}warehouse").roles("WHUSER")
                .and()
                .withUser("supplier").password("{noop}supp").roles("SUPPLIER")
                .and()
                .withUser("admin").password("{noop}admin").roles("ADMIN", "WHUSER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .regexMatchers(HttpMethod.POST, "/(warehouse|store)/register").hasRole("ADMIN")
                .regexMatchers(HttpMethod.POST, "/store/inventory/show/\\{goodsId}/\\{storeId}").hasRole("ADMIN")
                .regexMatchers(HttpMethod.GET, "/(warehouse|transaction|goods)/show/all").hasRole("ADMIN")
                .regexMatchers(HttpMethod.GET, "/store/show/all").hasRole("WHUSER")
                .regexMatchers(HttpMethod.GET, "/warehouse/show/(all|\\{warehouseName})").hasRole("WHUSER")
                .regexMatchers(HttpMethod.GET, "/transaction/show/(all|\\{warehouse})").hasRole("WHUSER")
                .regexMatchers(HttpMethod.GET, "/supply/\\{goodsId}/to/\\{warehouseId}").hasRole("SUPPLIER")
                .regexMatchers(HttpMethod.GET, "/warehouse/inventory/show/(all|\\{warehouse}|\\{warehouseId}/\\{goodsId})").hasRole("WHUSER")
                .regexMatchers(HttpMethod.POST, "/warehouse/delivery/\\{warehouseSrc}/\\{goodsId}/to/\\{storeDest}").hasRole("WHUSER")
                .regexMatchers(HttpMethod.POST, "/warehouse/transfer/\\{warehouseSrc}/\\{goodsId}/to/\\{warehouseDest}").hasRole("WHUSER")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }
}
