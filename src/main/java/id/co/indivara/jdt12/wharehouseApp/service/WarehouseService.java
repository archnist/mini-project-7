package id.co.indivara.jdt12.wharehouseApp.service;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface WarehouseService {
    ResponseEntity<Warehouse> warehouseRegister(Warehouse warehouse);
    List<Warehouse> showAllWarehouses();
    List<Warehouse> findByWarehouseName(String warehouseName);
    ResponseEntity<Warehouse> updateWarehouse(Warehouse warehouseId,Warehouse warehouseData);
}
