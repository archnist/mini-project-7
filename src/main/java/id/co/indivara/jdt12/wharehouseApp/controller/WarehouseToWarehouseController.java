package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.WarehouseToWarehouse;
import id.co.indivara.jdt12.wharehouseApp.service.WhouseToWhouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@CrossOrigin("http://localhost:8081")
@RequestMapping("/warehouse")
@RestController
public class WarehouseToWarehouseController {
    @Autowired
    WhouseToWhouseService whouseToWhouseService;

    @PostMapping("/transfer/{warehouseSrc}/{goodsId}/to/{warehouseDest}")
    public ResponseEntity<WarehouseToWarehouse> transferToWarehouse(@PathVariable("warehouseSrc")Warehouse warehouseSrc,@PathVariable("goodsId")Goods goodsId,@PathVariable("warehouseDest")Warehouse warehouseDest,@RequestBody WarehouseToWarehouse transfer){
        return new ResponseEntity<>(whouseToWhouseService.transferToWarehouse(goodsId,warehouseSrc,warehouseDest,transfer).getBody(),HttpStatus.CREATED);
    }
}