    package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.SuppToWarehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.service.SuppToWarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("http://localhost:8081")
@RequestMapping("/supply")
@RestController
public class SuppToWarehouseController {
    @Autowired
    SuppToWarehouseService suppToWarehouseService;

    @PostMapping("/{goodsId}/to/{warehouseId}")
    public ResponseEntity<SuppToWarehouse> supplyToWarehouse(@PathVariable("goodsId")Goods goodsId, @PathVariable("warehouseId")Warehouse warehouseId,@RequestBody SuppToWarehouse supply){
        return new ResponseEntity<>(suppToWarehouseService.supplyToWarehouse(goodsId,warehouseId,supply).getBody(), HttpStatus.OK);
    }
}