package id.co.indivara.jdt12.wharehouseApp.service;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Transaction;

public interface TransactionService {
    Transaction recordSupplyTransaction(Goods goodsId);
    Transaction recordTransferTransaction(Goods goodsId);
    Transaction recordDeliveryTransaction(Goods goodsId);
}
