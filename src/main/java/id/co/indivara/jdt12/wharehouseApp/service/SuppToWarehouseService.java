package id.co.indivara.jdt12.wharehouseApp.service;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.SuppToWarehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SuppToWarehouseService {
    ResponseEntity<SuppToWarehouse> supplyToWarehouse(Goods goodsId, Warehouse warehouseId,SuppToWarehouse supply);
}
