package id.co.indivara.jdt12.wharehouseApp.service;

import id.co.indivara.jdt12.wharehouseApp.entity.*;
import org.springframework.http.ResponseEntity;

public interface WarehouseInventoryService {
    ResponseEntity<WarehouseInventory> saveSupplyTransaction(Goods goodsId, Warehouse warehouseId, SuppToWarehouse supply);
    ResponseEntity<WarehouseInventory> saveTransferTransaction(Goods goodsId, Warehouse warehouseSrc, Warehouse warehouseDest, WarehouseToWarehouse transfer);
    ResponseEntity<WarehouseInventory> saveDeliveryTransaction(Goods goodsId,Warehouse warehouseSrc,Store storeDest,WarehouseToStore delivery);
}
