package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Store;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.WarehouseToStore;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseToStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("http://localhost:8080")
@RequestMapping("/warehouse")
@RestController
public class WarehouseToStoreController {
    @Autowired
    WarehouseToStoreService warehouseToStoreService;

    @PostMapping("/delivery/{warehouseSrc}/{goodsId}/to/{storeDest}")
    public ResponseEntity<WarehouseToStore> deliveryToStore(@PathVariable("warehouseSrc")Warehouse warehouseSrc, @PathVariable("goodsId")Goods goodsId,@PathVariable("storeDest")Store storeDest,@RequestBody WarehouseToStore delivery){
        return new ResponseEntity<>(warehouseToStoreService.deliveryToStore(goodsId,warehouseSrc,storeDest,delivery).getBody(), HttpStatus.OK);
    }
}