package id.co.indivara.jdt12.wharehouseApp.impl;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Transaction;
import id.co.indivara.jdt12.wharehouseApp.repo.TransactionRepository;
import id.co.indivara.jdt12.wharehouseApp.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public Transaction recordSupplyTransaction(Goods goodsId) {
        Transaction transaction = new Transaction();
        transaction.setGoods(goodsId);
        transaction.setType("Supply To Warehouse");
        transaction.setTransactionCode("T" + (transactionRepository.count() + 1));
        transaction.setDateTime(new Date());
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction recordTransferTransaction(Goods goodsId) {
        Transaction transaction = new Transaction();
        transaction.setGoods(goodsId);
        transaction.setType("Transfer To Warehouse");
        transaction.setTransactionCode("T" + (transactionRepository.count() + 1));
        transaction.setDateTime(new Date());
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction recordDeliveryTransaction(Goods goodsId) {
        Transaction transaction = new Transaction();
        transaction.setGoods(goodsId);
        transaction.setType("Delivery To Store");
        transaction.setTransactionCode("T" + (transactionRepository.count() + 1));
        transaction.setDateTime(new Date());
        return transactionRepository.save(transaction);
    }
}
