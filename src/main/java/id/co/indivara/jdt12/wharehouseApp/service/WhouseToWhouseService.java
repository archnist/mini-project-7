package id.co.indivara.jdt12.wharehouseApp.service;

import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.WarehouseToWarehouse;
import org.springframework.http.ResponseEntity;


public interface WhouseToWhouseService {
    ResponseEntity<WarehouseToWarehouse> transferToWarehouse(Goods goods, Warehouse warehouseSrc,Warehouse warehouseDest,WarehouseToWarehouse transfer);
}
