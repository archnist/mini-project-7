package id.co.indivara.jdt12.wharehouseApp.impl;

import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.repo.GoodsRepository;
import id.co.indivara.jdt12.wharehouseApp.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    GoodsRepository goodsRepository;

    @Override
    public ResponseEntity<Goods> addGoods(Goods goods) {
        return new ResponseEntity<>(goodsRepository.save(goods), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Goods> goodsUpdate(Goods goodsId, Goods goodsData) {
        Goods goods = goodsRepository.findById(goodsId.getGoodsId()).get();
        goods.setGoodsName(goodsData.getGoodsName());
        return new ResponseEntity<>(goodsRepository.save(goods),HttpStatus.CREATED);
    }

    @Override
    public List<Goods> showAllGoods() {
        return (List<Goods>) goodsRepository.findAll();
    }
}
