package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:8081")
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    GoodsService goodsService;

    @PostMapping("/add")
    public ResponseEntity<Goods> addGoods(@RequestBody Goods goods){
        return new ResponseEntity<>(goodsService.addGoods(goods).getBody(), HttpStatus.CREATED);
    }
    @PutMapping("/update/{goodsId}")
    public ResponseEntity<Goods> goodsUpdate(@PathVariable("goodsId") Goods goodsId, @RequestBody Goods goodsData){
        return new ResponseEntity<>(goodsService.goodsUpdate(goodsId,goodsData).getBody(),HttpStatus.CREATED);
    }
    @GetMapping("/show/all")
    public List<Goods> showAllGoods(){
        return (List<Goods>) goodsService.showAllGoods();
    }
}
