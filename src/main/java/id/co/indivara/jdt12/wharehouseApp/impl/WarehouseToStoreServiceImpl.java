package id.co.indivara.jdt12.wharehouseApp.impl;
import id.co.indivara.jdt12.wharehouseApp.entity.*;
import id.co.indivara.jdt12.wharehouseApp.repo.TransactionRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseToStoreRepository;
import id.co.indivara.jdt12.wharehouseApp.service.TransactionService;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseInventoryService;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseToStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class WarehouseToStoreServiceImpl implements WarehouseToStoreService {
    @Autowired
    WarehouseToStoreRepository warehouseToStoreRepository;
    @Autowired
    TransactionService transactionService;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    WarehouseInventoryService warehouseInventoryService;
    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;

    @Override
    public ResponseEntity<WarehouseToStore> deliveryToStore(Goods goodsId, Warehouse warehouseSrc, Store storeDest, WarehouseToStore delivery) {
        WarehouseInventory warehouseSource = warehouseInventoryRepository.findByGoodsAndWarehouse(goodsId,warehouseSrc);
        WarehouseToStore warehouseToStore = new WarehouseToStore();
        if (warehouseSource.getStock() >= delivery.getTotal()) {
            warehouseInventoryService.saveDeliveryTransaction(goodsId, warehouseSrc, storeDest, delivery);
            warehouseToStore.setTransactionCode("T" + (transactionRepository.count() + 1));
            warehouseToStore.setWarehouseSrc(warehouseSrc);
            warehouseToStore.setStoreDest(storeDest);
            warehouseToStore.setGoods(goodsId);
            warehouseToStore.setTotal(delivery.getTotal());
            warehouseToStore.setDateTime(new Date());
            transactionService.recordDeliveryTransaction(goodsId);
        }
        return new ResponseEntity<>(warehouseToStoreRepository.save(warehouseToStore),HttpStatus.CREATED);
    }
}
