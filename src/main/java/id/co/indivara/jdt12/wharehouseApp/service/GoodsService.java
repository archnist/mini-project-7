package id.co.indivara.jdt12.wharehouseApp.service;

import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface GoodsService {
    ResponseEntity<Goods> addGoods(Goods goods);
    ResponseEntity<Goods> goodsUpdate(Goods goodsId,Goods goodsData);
    List<Goods> showAllGoods();
}
