package id.co.indivara.jdt12.wharehouseApp.impl;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.WarehouseInventory;
import id.co.indivara.jdt12.wharehouseApp.entity.WarehouseToWarehouse;
import id.co.indivara.jdt12.wharehouseApp.exception.NotFoundException;
import id.co.indivara.jdt12.wharehouseApp.repo.TransactionRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseToWarehouseRepository;
import id.co.indivara.jdt12.wharehouseApp.service.TransactionService;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseInventoryService;
import id.co.indivara.jdt12.wharehouseApp.service.WhouseToWhouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class WhouseToWhouseServiceImpl implements WhouseToWhouseService {
    @Autowired
    WarehouseToWarehouseRepository warehouseToWarehouseRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    TransactionService transactionService;
    @Autowired
    WarehouseInventoryService warehouseInventoryService;
    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;

    @Override
    public ResponseEntity<WarehouseToWarehouse> transferToWarehouse(Goods goods, Warehouse warehouseSrc, Warehouse warehouseDest, WarehouseToWarehouse transfer) {
        WarehouseInventory warehouseSource = warehouseInventoryRepository.findByGoodsAndWarehouse(goods,warehouseSrc);
        WarehouseToWarehouse warehouseToWarehouse = new WarehouseToWarehouse();
        if (warehouseSource.getStock() >= transfer.getTotal()) {
            warehouseInventoryService.saveTransferTransaction(goods, warehouseSrc, warehouseDest, transfer);
            warehouseToWarehouse.setTransactionCode("T" + (transactionRepository.count() + 1));
            warehouseToWarehouse.setWarehouseSource(warehouseSrc);
            warehouseToWarehouse.setWarehouseDestination(warehouseDest);
            warehouseToWarehouse.setGoods(goods);
            warehouseToWarehouse.setTotal(transfer.getTotal());
            warehouseToWarehouse.setDateTime(new Date());
            transactionService.recordTransferTransaction(goods);
        } else {
            throw new NotFoundException("Stock tidak mencukupi");
        }
        return new ResponseEntity<>(warehouseToWarehouseRepository.save(warehouseToWarehouse), HttpStatus.CREATED);
    }
}
