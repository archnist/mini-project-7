package id.co.indivara.jdt12.wharehouseApp.service;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Store;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.WarehouseToStore;
import org.springframework.http.ResponseEntity;

public interface WarehouseToStoreService {
    ResponseEntity<WarehouseToStore> deliveryToStore(Goods goodsId, Warehouse warehouseSrc, Store storeDest, WarehouseToStore delivery);
}
