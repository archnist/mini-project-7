package id.co.indivara.jdt12.wharehouseApp.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "warehouse_to_warehouse")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WarehouseToWarehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "transaction_code")
    private String transactionCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "whouse_src")
    @JsonIgnoreProperties({"hibernateLazyInitializer"})
    private Warehouse warehouseSource;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "whouse_dest")
    @JsonIgnoreProperties({"hibernateLazyInitializer"})
    private Warehouse warehouseDestination;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "goods_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer"})
    private Goods goods;

    @Column(name = "total")
    private Integer total;
    @Column(name = "date_time")
    private Date dateTime;
}
