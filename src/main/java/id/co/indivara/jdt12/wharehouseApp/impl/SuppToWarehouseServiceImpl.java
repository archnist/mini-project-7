package id.co.indivara.jdt12.wharehouseApp.impl;

import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.SuppToWarehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.Transaction;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.repo.SuppToWarehouseRepository;
import id.co.indivara.jdt12.wharehouseApp.repo.TransactionRepository;
import id.co.indivara.jdt12.wharehouseApp.service.SuppToWarehouseService;
import id.co.indivara.jdt12.wharehouseApp.service.TransactionService;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SuppToWarehouseServiceImpl implements SuppToWarehouseService {
    @Autowired
    SuppToWarehouseRepository suppToWarehouseRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    TransactionService transactionService;
    @Autowired
    WarehouseInventoryService warehouseInventoryService;

    @Override
    public ResponseEntity<SuppToWarehouse> supplyToWarehouse(Goods goodsId, Warehouse warehouseId,SuppToWarehouse supply) {
        warehouseInventoryService.saveSupplyTransaction(goodsId,warehouseId,supply);
        SuppToWarehouse suppToWarehouse = new SuppToWarehouse();
        suppToWarehouse.setTransactionCode("T" + (transactionRepository.count() + 1));
        suppToWarehouse.setGoods(goodsId);
        suppToWarehouse.setWarehouse(warehouseId);
        suppToWarehouse.setTotal(supply.getTotal());
        suppToWarehouse.setDateTime(new Date());
        transactionService.recordSupplyTransaction(goodsId);
        return new ResponseEntity<>(suppToWarehouseRepository.save(suppToWarehouse),HttpStatus.CREATED);
    }
}
