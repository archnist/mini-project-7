package id.co.indivara.jdt12.wharehouseApp.service;

import id.co.indivara.jdt12.wharehouseApp.entity.Store;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface StoreService {
    ResponseEntity<Store> storeRegister(Store store);
    ResponseEntity<Store> storeUpdate(Store storeId, Store storeData);
    List<Store> showAllStores();
}
