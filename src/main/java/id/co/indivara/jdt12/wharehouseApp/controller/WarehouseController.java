package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.*;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseRepository;
import id.co.indivara.jdt12.wharehouseApp.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/warehouse")
public class WarehouseController {
    @Autowired
    WarehouseService warehouseService;

    @PostMapping("/register")
    public ResponseEntity<Warehouse> warehouseRegister(@RequestBody Warehouse warehouse){
        return new ResponseEntity<>(warehouseService.warehouseRegister(warehouse).getBody(), HttpStatus.OK);
    }
    @GetMapping("/show/all")
    public List<Warehouse> showAllWarehouses(){
        return (List<Warehouse>) warehouseService.showAllWarehouses();
    }
    @PutMapping("/update/{warehouseId}")
    public ResponseEntity<Warehouse> updateWarehouse(@PathVariable("warehouseId") Warehouse warehouseId, @RequestBody Warehouse warehouseData){
        return new ResponseEntity<>(warehouseService.updateWarehouse(warehouseId,warehouseData).getBody(),HttpStatus.CREATED);
    }
    @GetMapping("/show/{warehouseName}")
    public List<Warehouse> findByName(@PathVariable("warehouseName") String warehouseName){
        return (List<Warehouse>) warehouseService.findByWarehouseName(warehouseName);
    }
}
