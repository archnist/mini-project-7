package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.Goods;
import id.co.indivara.jdt12.wharehouseApp.entity.Warehouse;
import id.co.indivara.jdt12.wharehouseApp.entity.WarehouseInventory;
import id.co.indivara.jdt12.wharehouseApp.repo.WarehouseInventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:8081")
@RequestMapping("/warehouse")
@RestController
public class WarehouseInventoryController {
    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;

    @GetMapping("/inventory/show/all")
    public List<WarehouseInventory> showAll(){
        return (List<WarehouseInventory>) warehouseInventoryRepository.findAll();
    }

    @GetMapping("/inventory/show/{warehouse}")
    public List<WarehouseInventory> findByWarehouse(@PathVariable("warehouse") Warehouse warehouse){
        return (List<WarehouseInventory>) warehouseInventoryRepository.findByWarehouse(warehouse);
    }

    @GetMapping("/inventory/show/{warehouseId}/{goodsId}")
    public WarehouseInventory findByGoodsAndWarehouse(@PathVariable("warehouseId")Warehouse warehouse, @PathVariable("goodsId")Goods goods){
        return warehouseInventoryRepository.findByGoodsAndWarehouse(goods,warehouse);
    }
}
