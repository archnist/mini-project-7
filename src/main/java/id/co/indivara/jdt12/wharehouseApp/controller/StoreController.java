package id.co.indivara.jdt12.wharehouseApp.controller;
import id.co.indivara.jdt12.wharehouseApp.entity.Store;
import id.co.indivara.jdt12.wharehouseApp.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/store")
public class StoreController {
    @Autowired
    StoreService storeService;

    @PostMapping("/register")
    public ResponseEntity<Store> storeRegister(@RequestBody Store store){
        return new ResponseEntity<>(storeService.storeRegister(store).getBody(),HttpStatus.OK);
    }

    @PutMapping("/update/{storeId}")
    public ResponseEntity<Store> storeUpdate(@PathVariable("storeId") Store storeId, @RequestBody Store storeData){
        return new ResponseEntity<>(storeService.storeUpdate(storeId,storeData).getBody(),HttpStatus.CREATED);
    }

    @GetMapping("/show/all")
    public List<Store> showAllStores(){
        return (List<Store>) storeService.showAllStores();
    }
}


